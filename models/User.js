const mongoose = require('mongoose');

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    avatar: {
      type: String,
    },
    createdAt: { type: Date, default: Date.now },
  },
);

module.exports = mongoose.model('User', schema);
