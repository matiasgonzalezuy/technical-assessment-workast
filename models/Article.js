const mongoose = require('mongoose');

const schema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User Id',
      required: true,
    },
    title: {
      type: String,
      ref: 'Title',
    },
    text: {
      type: String,
      ref: 'Text',
    },
    tags: {
      type: [String],
      ref: 'Tags',
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
);

module.exports = mongoose.model('Article', schema);
