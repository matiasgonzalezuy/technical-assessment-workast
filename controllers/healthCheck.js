
function health(_req, res) {
  try {
    res.status(200).send(({ message: 'Hi, I am fine thanks!' }));
  } catch (error) {
    res.status(500).send((error));
  }
}

function isAlive(_req, res) {
  try {
    res.status(200).send(({ api: 'Api Rest Technical Assessment', version: '1.0.1' }));
  } catch (error) {
    res.status(500).send((error));
  }
}

module.exports = {
  health,
  isAlive,
};
