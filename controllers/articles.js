const ArticleModel = require('../models/Article');

async function getArticles(_req, res) {
  try {
    const articles = await ArticleModel.find();
    res.send(articles);
  } catch (error) {
    res.send(500, error);
  }
}

async function newArticle(req, res) {
  const article = new ArticleModel(req.body);
  article.save((err) => {
    if (err) return res.status(500).send({ message: `Error when create a new Article:${err}` });
    try {
      return res.status(200).send({ article });
    } catch (error) {
      return error;
    }
  });
}

async function editArticle(req, res) {
  try {
    const { id } = req.params;
    const data = req.body;
    const article = await ArticleModel.findOne({ _id: id });
    if (article) {
      article.userId = data.userId;
      article.title = data.title;
      article.text = data.text;
      article.tags = data.tags;
      article.save((err) => {
        if (err) res.status(500).send({ message: `Error when edit Article:${err}` });
        try {
          return res.status(200).send({ message: 'Article updated ', article });
        } catch (error) {
          return error;
        }
      });
    } else {
      res.status(500).send({ message: 'The Article id not exist' });
    }
  } catch (error) {
    res.status(500).send((error));
  }
}

async function getArtilceById(req, res) {
  try {
    const { id } = req.params;
    const article = await ArticleModel.findOne({ _id: id });
    if (article) {
      res.status(200).send((article));
    } else {
      res.status(500).send({ message: 'The article id not exist' });
    }
  } catch (error) {
    res.status(500).send((error));
  }
}

async function getArtilcesByUserId(req, res) {
  try {
    const { id } = req.params;
    const articles = await ArticleModel.find({ userId: id });
    if (articles) {
      res.status(200).send((articles));
    } else {
      res.status(500).send({ message: 'The article id not exist' });
    }
  } catch (error) {
    res.status(500).send((error));
  }
}

async function getArtilcesByTags(req, res) {
  try {
    const { tags } = req.body;
    const articles = await ArticleModel.find({ tags: { $in: tags } });
    if (articles) {
      res.status(200).send((articles));
    } else {
      res.status(500).send({ message: 'The article id not exist' });
    }
  } catch (error) {
    res.status(500).send((error));
  }
}

async function deleteArticle(req, res) {
  const { id } = req.params;
  await ArticleModel.remove({ _id: id });
  res.status(200).send({ message: 'Article deleted ' });
}

module.exports = {
  getArticles,
  getArtilceById,
  editArticle,
  newArticle,
  deleteArticle,
  getArtilcesByUserId,
  getArtilcesByTags,
};
