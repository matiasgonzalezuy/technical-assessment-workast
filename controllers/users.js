
const UserModel = require('../models/User');

async function getUsers(_req, res) {
  try {
    const users = await UserModel.find();
    return res.status(200).send(users);
  } catch (error) {
    return res.status(500).send(error);
  }
}

async function editUser(req, res) {
  const { id } = req.params;
  const data = req.body;
  const user = await UserModel.findOne({ _id: id });
  if (user) {
    user.name = data.name;
    user.avatar = data.avatar;
    await user.save((err) => {
      if (err) return res.status(500).send({ message: `Error when try to edit User:${err}` });
      try {
        return res.status(200).send({ message: 'User updated ', user });
      } catch (error) {
        return error;
      }
    });
  } else {
    res.status(500).send({ message: 'The user id not exist' });
  }
}

async function newUser(req, res) {
  const user = new UserModel(req.body);
  await user.save((err) => {
    if (err) return res.status(500).send({ message: `Error when create a new User:${err}` });
    try {
      return res.status(200).send({ user });
    } catch (error) {
      return error;
    }
  });
}

async function getUserById(req, res) {
  try {
    const { id } = req.params;
    const user = await UserModel.findOne({ _id: id });
    if (user) {
      return res.status(200).send((user));
    }
    return res.status(500).send({ message: 'The user id not exist' });
  } catch (error) {
    return res.status(500).send((error));
  }
}

async function deleteUser(req, res) {
  const { id } = req.params;
  await UserModel.remove({ _id: id });
  return res.status(200).send({ message: 'User deleted ' });
}

module.exports = {
  getUsers,
  getUserById,
  editUser,
  newUser,
  deleteUser,
};
