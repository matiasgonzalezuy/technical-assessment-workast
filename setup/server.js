const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const server = express();

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');

const indexRoutes = require('../routes/index');

server.set('port', process.env.PORT || 3000);

server.use(morgan('dev'));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(express.static('public'));
server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

server.use(cors({
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
}));

server.use('/', indexRoutes);

module.exports = server;
