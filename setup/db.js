require('dotenv').config();
const mongoose = require('mongoose');
// eslint-disable-next-line import/no-extraneous-dependencies
const { Mockgoose } = require('mockgoose');

const {
  DB_USER,
  DB_PASS,
  DB_HOST,
  DB_PORT,
  DB_NAME,
  NODE_ENV,
} = process.env;


function connect() {
  return new Promise((resolve, reject) => {
    if (NODE_ENV === 'test') {
      const mockgoose = new Mockgoose(mongoose);
      mockgoose.prepareStorage()
        .then(() => {
          mongoose
            .connect(`mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`, {
              useNewUrlParser: true,
              useUnifiedTopology: true,
              useCreateIndex: true,
            })
            .then((res, err) => {
              if (err) return reject(err);
              return resolve();
            })
            .catch(err => reject(err));
        });
    } else {
      mongoose
        .connect(`mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useCreateIndex: true,
        })
        .then((res, err) => {
          if (err) return reject(err);
          return resolve();
        })
        .catch(err => reject(err));
    }
  });
}


function close() {
  return mongoose.disconnect();
}

module.exports = {
  connect,
  close,
};
