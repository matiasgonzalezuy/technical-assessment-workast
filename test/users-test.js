/* eslint-disable no-undef */
process.env.NODE_ENV = 'test';
const { expect } = require('chai');
const request = require('supertest');
const mongoose = require('mongoose');

const app = require('../setup/server');
const db = require('../setup/db');


describe('Test Users end points', () => {
  const newUser = {
    name: 'matias',
    avatar: 'http://avatar.com/image.png',
  };
  let idUser;
  before((done) => {
    db.connect()
      .then(() => done())
      .catch(err => done(err));
  });

  after((done) => {
    db.close()
      .then(() => done())
      .catch(err => done(err));
  });

  it('Check Add new User in /new-user', (done) => {
    request(app)
      .post('/new-user')
      .send(newUser)
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const { user } = res.body;
        const { _id } = user;
        idUser = _id;
        expect(user).to.contain.property('_id');
        expect(user).to.contain.property('name');
        expect(user).to.contain.property('avatar');
        expect(user).to.contain.property('createdAt');
        done();
      })
      .catch(err => done(err));
  });

  it('Check Edit User in /user/:id', (done) => {
    newUser.name = 'Matias';
    request(app)
      .put(`/user/${idUser}`)
      .send(newUser)
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const { user } = res.body;
        expect(user).to.contain.property('_id');
        expect(user).to.contain.property('name');
        expect(user).to.contain.property('avatar');
        expect(user).to.contain.property('createdAt');
        expect(user.name).to.equal('Matias');
        done();
      })
      .catch(err => done(err));
  });

  it('Check Get User by ID in /user/:id', (done) => {
    request(app)
      .get(`/user/${idUser}`)
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const user = res.body;
        expect(user).to.contain.property('_id');
        expect(user).to.contain.property('name');
        expect(user).to.contain.property('avatar');
        expect(user).to.contain.property('createdAt');
        done();
      })
      .catch(err => done(err));
  });

  it('Check Get all Users in /users', (done) => {
    request(app)
      .get('/users')
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const user = res.body[0];
        expect(res.body).to.be.an('array');
        expect(user).to.contain.property('_id');
        expect(user).to.contain.property('name');
        expect(user).to.contain.property('avatar');
        expect(user).to.contain.property('createdAt');
        done();
      })
      .catch(err => done(err));
  });
  it('Check Delete User by _id /user/:id', (done) => {
    request(app)
      .delete(`/user/${idUser}`)
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const { message } = res.body;
        expect(res.body).to.contain.property('message');
        expect(message).to.equal('User deleted ');
        done();
      })
      .catch(err => done(err));
  });
});
