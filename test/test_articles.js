/* eslint-disable no-undef */
process.env.NODE_ENV = 'test';
const { expect } = require('chai');
const request = require('supertest');
const mongoose = require('mongoose');

const app = require('../setup/server');
const db = require('../setup/db');


describe('Test Articles end points', () => {
  const newArticle = {
    userId: mongoose.Types.ObjectId('4edd40c86762e0fb12000003'),
    title: 'title-test',
    text: 'Text Description',
    tags: ['tag1', 'tag2', 'tag3'],
  };
  let idArticle;
  before((done) => {
    db.connect()
      .then(() => done())
      .catch(err => done(err));
  });

  after((done) => {
    db.close()
      .then(() => done())
      .catch(err => done(err));
  });

  it('Check Add new Article in /new-article', (done) => {
    request(app)
      .post('/new-article')
      .send(newArticle)
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const { article } = res.body;
        const { _id } = article;
        idArticle = _id;
        expect(article).to.contain.property('_id');
        expect(article).to.contain.property('userId');
        expect(article).to.contain.property('title');
        expect(article).to.contain.property('text');
        expect(article).to.contain.property('tags');
        expect(article).to.contain.property('createdAt');
        done();
      })
      .catch(err => done(err));
  });

  it('Check Edit Article in /article/:id', (done) => {
    newArticle.title = 'New Title';
    request(app)
      .put(`/article/${idArticle}`)
      .send(newArticle)
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const { article } = res.body;
        expect(article).to.contain.property('_id');
        expect(article).to.contain.property('userId');
        expect(article).to.contain.property('title');
        expect(article).to.contain.property('text');
        expect(article).to.contain.property('tags');
        expect(article.title).to.equal('New Title');
        expect(article).to.contain.property('createdAt');
        done();
      })
      .catch(err => done(err));
  });

  it('Check Get all Articles in /articles', (done) => {
    request(app)
      .get('/articles')
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const article = res.body[0];
        expect(res.body).to.be.an('array');
        expect(article).to.contain.property('_id');
        expect(article).to.contain.property('userId');
        expect(article).to.contain.property('title');
        expect(article).to.contain.property('text');
        expect(article).to.contain.property('tags');
        expect(article).to.contain.property('createdAt');
        done();
      })
      .catch(err => done(err));
  });

  it('Check Get all Articles by tag || tags /articles-by-tags/', (done) => {
    const tags = { tags: ['tag1', 'tag2'] };
    request(app)
      .post('/articles-by-tags/')
      .set('authorization', process.env.API_TOKEN)
      .send(tags)
      .then((res) => {
        const article = res.body[0];
        expect(res.body).to.be.an('array');
        expect(article).to.contain.property('_id');
        expect(article).to.contain.property('userId');
        expect(article).to.contain.property('title');
        expect(article).to.contain.property('text');
        expect(article).to.contain.property('tags');
        expect(article).to.contain.property('createdAt');
        done();
      })
      .catch(err => done(err));
  });

  it('Check Get all Articles by UserId /article-by-user-id/:id', (done) => {
    request(app)
      .get(`/article-by-user-id/${newArticle.userId}`)
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const article = res.body[0];
        expect(res.body).to.be.an('array');
        expect(article.userId).to.equal(newArticle.userId.toHexString());
        expect(article).to.contain.property('_id');
        expect(article).to.contain.property('userId');
        expect(article).to.contain.property('title');
        expect(article).to.contain.property('text');
        expect(article).to.contain.property('tags');
        expect(article).to.contain.property('createdAt');
        done();
      })
      .catch(err => done(err));
  });

  it('Check Delete Article by _id /article/:id', (done) => {
    request(app)
      .delete(`/article/${idArticle}`)
      .set('authorization', process.env.API_TOKEN)
      .then((res) => {
        const { message } = res.body;
        expect(res.body).to.contain.property('message');
        expect(message).to.equal('Article deleted ');
        done();
      })
      .catch(err => done(err));
  });
});
